install_powerdns:
  pkg.installed:
    - pkgs:
      - pdns-server
      - pdns-backend-mysql
