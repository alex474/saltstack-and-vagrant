# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  config.vm.define :master do |master_config|
   master_config.vm.box = "ubuntu/trusty64"
   master_config.vm.host_name = 'master.local'
   master_config.vm.network "private_network", ip: "192.168.10.10"
   master_config.vm.synced_folder "saltstack/salt/", "/srv/salt"
   master_config.vm.synced_folder "saltstack/pillar/", "/srv/pillar"

   master_config.vm.provision :salt do |salt|
    salt.master_config = "saltstack/etc/master"
    salt.master_key = "saltstack/keys/master_minion.pem"
    salt.master_pub = "saltstack/keys/master_minion.pub"
    salt.minion_key = "saltstack/keys/master_minion.pem"
    salt.minion_pub = "saltstack/keys/master_minion.pub"
    salt.seed_master = {
                        "minion1" => "saltstack/keys/minion.pub",
                        "minion2" => "saltstack/keys/minion.pub",
                        "minion3" => "saltstack/keys/minion.pub",
                        "minion4" => "saltstack/keys/minion.pub"
                       }

    salt.install_type = "stable"
    salt.install_master = true
    salt.no_minion = true
    salt.verbose = true
    salt.colorize = true
    salt.bootstrap_options = "-P -c /tmp"
  end
end

config.vm.define :minion1 do |minion_config|
  minion_config.vm.box = "ubuntu/trusty64"
  minion_config.vm.host_name = 'minion1.local'
  minion_config.vm.network "private_network", ip: "192.168.10.11"

  minion_config.vm.provision :salt do |salt|
    salt.minion_config = "saltstack/etc/minion1"
    salt.minion_key = "saltstack/keys/minion.pem"
    salt.minion_pub = "saltstack/keys/minion.pub"
    salt.install_type = "stable"
    salt.verbose = true
    salt.colorize = true
    salt.run_highstate = true
    salt.bootstrap_options = "-P -c /tmp"
  end
end

config.vm.define :minion2 do |minion_config|
  minion_config.vm.box = "ubuntu/trusty64"
  minion_config.vm.host_name = 'minion2.local'
  minion_config.vm.network "private_network", ip: "192.168.10.12"

  minion_config.vm.provision :salt do |salt|
    salt.minion_config = "saltstack/etc/minion2"
    salt.minion_key = "saltstack/keys/minion.pem"
    salt.minion_pub = "saltstack/keys/minion.pub"
    salt.install_type = "stable"
    salt.verbose = true
    salt.colorize = true
    salt.bootstrap_options = "-P -c /tmp"
  end
end

config.vm.define :minion3 do |minion_config|
  minion_config.vm.box = "ubuntu/trusty64"
  minion_config.vm.host_name = 'minion3.local'
  minion_config.vm.network "private_network", ip: "192.168.10.13"

  minion_config.vm.provision :salt do |salt|
    salt.minion_config = "saltstack/etc/minion3"
    salt.minion_key = "saltstack/keys/minion.pem"
    salt.minion_pub = "saltstack/keys/minion.pub"
    salt.install_type = "stable"
    salt.verbose = true
    salt.colorize = true
    salt.bootstrap_options = "-P -c /tmp"
  end
end

config.vm.define :minion4 do |minion_config|
  minion_config.vm.box = "ubuntu/trusty64"
  minion_config.vm.host_name = 'minion4.local'
  minion_config.vm.network "private_network", ip: "192.168.10.14"

  minion_config.vm.provision :salt do |salt|
    salt.minion_config = "saltstack/etc/minion4"
    salt.minion_key = "saltstack/keys/minion.pem"
    salt.minion_pub = "saltstack/keys/minion.pub"
    salt.install_type = "stable"
    salt.verbose = true
    salt.colorize = true
    salt.bootstrap_options = "-P -c /tmp"
  end
 end
end
